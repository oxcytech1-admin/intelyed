package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import base.PageBase;


public class HomePage extends PageBase  {
	
	@FindBy(xpath ="//body/div[@id='root']/div/div/div/div/div/div/div/ul/div[3]")
    private WebElement lnkMySchool;

	@FindBy(xpath ="//body/div[@id='root']/div/div/div/div/div/div/div/ul/div/div/div/div/div[1]")
    private WebElement lnkAchievement;
	
	@FindBy(xpath ="//button[@class='MuiButtonBase-root MuiIconButton-root MuiIconButton-edgeStart']")
    private WebElement btnSettings;
	
	@FindBy(xpath ="//body//div[@id='root']//div//div//div//div//div//div//ul//div//div[3]")
    private WebElement lnkPLCminutes;
	
	@FindBy(xpath ="//body/div[@id='root']/div/div/div/div/div/div/div/ul/div[2]")
    private WebElement lnkMyTeams;
	
	@FindBy(xpath ="//body/div/div/div[@role='alert']/div/span[1]")
    private WebElement getConfirmationMessage;
	
	@FindBy(xpath ="//div[@role='tabpanel']//span[1]//*[local-name()='svg']")
    private WebElement btnNewMinutes;
	
	//Class setting
	@FindBy(xpath ="//body//div//header//button[2]")
	private WebElement btnSetting;
	
	@FindBy(xpath ="//body/div[@id='customized-menu']/div[3]/ul[1]/li[1]")
	private WebElement btnAdminSettings;
	
	public  HomePage(WebDriver driver) {
		setWebDriver(driver);
	}
	
	public void clickOnlnkMySchool() throws Throwable {
		lnkMySchool.click();
		smallwaitDriver();
	}
	
	public PLCTeamSetupPage clickOnbtnSettings() {
		waitForElementToAppear(btnSettings);
		btnSettings.click();
		//jsExecutorClickOn(lnkSettings);
		//lnkSettings.sendKeys(Keys.ENTER);
		waitForPageLoaded();
		return new PLCTeamSetupPage(pbDriver);
		
	}
	
	public AchievementPage clickOnlnkAchievement() {
		waitForElementToAppear(lnkAchievement);
		lnkAchievement.click();
		waitForPageLoaded();
		return new AchievementPage(pbDriver);
		
	}

	public PLCminutesPage clickOnlnkPLCminutes() {
		waitForElementToAppear(lnkPLCminutes);
		lnkPLCminutes.click();
		waitForPageLoaded();
		return new PLCminutesPage(pbDriver);
		
	}
	public PLCTaskPage clickOnlnkPLCminutes1() {
		waitForElementToAppear(lnkPLCminutes);
		lnkPLCminutes.click();
		waitForPageLoaded();
		return new PLCTaskPage(pbDriver);
		
	}

	public void clickOnlnklnkMyTeams() {
		waitForElementToAppear(lnkMyTeams);
		lnkMyTeams.click();
	}
	public String getConfirmationMessage() throws Throwable {

		waitForElementToAppear(getConfirmationMessage);
		return getConfirmationMessage.getText();
	}


	public PLCminutesPage clickOnbtnNewMinutes() {
		waitForElementToAppear(btnNewMinutes);
		btnNewMinutes.click();
		//jsExecutorClickOn(lnkSettings);
		//lnkSettings.sendKeys(Keys.ENTER);
		waitForPageLoaded();
		return new PLCminutesPage(pbDriver);
		
	}
	
	public void clickOnbtnSetting() throws Throwable {
		waitForElementToClickable(btnSetting);
		btnSetting.click();
		
	}
	public ClassSetupPage clickOnbtnAdminSettings() throws Throwable {
		waitForElementToClickable(btnAdminSettings);
		//btnAdminSettings.click();
		jsExecutorClickOn(btnAdminSettings);
		waitDriver();
		return new ClassSetupPage(pbDriver);
	}
	public PLCTeamSetupPage clickOnbtnAdminSettingsForTeams() throws Throwable {
		waitForElementToClickable(btnAdminSettings);
		//btnAdminSettings.click();
		jsExecutorClickOn(btnAdminSettings);
		waitDriver();
		return new PLCTeamSetupPage(pbDriver);
	}
}	

