package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import base.PageBase;
import utils.TestUtil;

public class LoginPage extends PageBase {
	
	@FindBy(xpath ="//button[@type='submit']")
    private WebElement btnLogin;
	
	@FindBy(xpath = "//input[@id='outlined-adornment-password']")
    private WebElement txtpassword;
	
	@FindBy(xpath = "//input[@id='username']")
    private WebElement txtuserName;

	@FindBy(xpath = "//div[@role='alert']")        
	private WebElement lblModalMessages;
	


	
	public LoginPage(WebDriver driver) {
		setWebDriver(driver);
		
	}
	
	public void enterUserName(String strUsername) throws Throwable {
		
		TestUtil.mouseHover(txtuserName);
		txtuserName.sendKeys(strUsername);
	}
	
	public void enterPassword(String strPassword) {
		txtpassword.sendKeys(strPassword);
	}
	
	public void clickOnbtnLogin() {
		btnLogin.click();
	}
	
	
	public HomePage intelyEDLogin(String userName, String password) throws Throwable {
		
		waitForPageLoaded();
		waitForElementToAppear(txtuserName);
		enterUserName(userName);
		enterPassword(password);
		clickOnbtnLogin();
		waitForPageLoaded();
		waitDriver();
		//System.out.println(pbDriver.getTitle());
		if(pbDriver.getTitle().equalsIgnoreCase("intelyEd")) {
			return new HomePage(pbDriver); 
		}else {
			return null;
		}
			
		

	}
	
	public String loginValidation() {
		return pbDriver.getTitle();
	}

	public String getConfirmationMessage() {
		waitForElementToAppear(lblModalMessages);
		return lblModalMessages.getText();
	}
	
	
}

