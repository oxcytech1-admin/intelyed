package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import base.PageBase;
import utils.TestUtil;

public class PLCTeamSetupPage extends PageBase {
	
	@FindBy(xpath ="//*[name()='path' and contains(@d,'M19.14 12.')]")
	private WebElement btnSetting;
	
	@FindBy(xpath ="//p[normalize-space()='Admin Settings']")
	private WebElement btnAdminSettings;

	@FindBy(xpath ="//input[@id='outlined-multiline-static']")
	private WebElement txtPLCTeamName;
	
	@FindBy(xpath ="//div[contains(text(),'Select Content Area')]")
	private WebElement liContentArea;

	@FindBy(xpath ="//label[3]")
	private WebElement btnradioSETDAYS;
	
	@FindBy(xpath ="//div[@class='MuiInputBase-root MuiInput-root MuiInput-underline Mui-focused Mui-focused MuiInputBase-formControl MuiInput-formControl']//input[@id='time']")
	private WebElement btnAlarmclock;
	
	@FindBy(xpath ="//div[@class='public-DraftStyleDefault-block public-DraftStyleDefault-ltr']")
	private WebElement txtTEAMNORMS;
	
	@FindBy(xpath ="//span[normalize-space()='Next']") //main//button[2]")
	private WebElement btnNEXT;
	
	@FindBy(xpath ="//form[1]//div[1]//div[1]//input[1]")
	private WebElement txtStartMeetingTime;
	
	@FindBy(xpath ="//form[2]//div[1]//div[1]//input[1]")
	private WebElement txtEndMeetingTime;
	
	@FindBy(xpath ="//body/div[@id='root']/div/div/main/div/div/div/div/div/div/div/div/div/div/div/div/div/div[@role='list']/div[3]")
	private WebElement selectMember;

	private String pstrStartMeetingTime;

	private String pstrEndMeetingTime;
	
	public PLCTeamSetupPage(WebDriver driver) {
		setWebDriver(driver);
	}

	

	public void clickOnbtnSetting() throws Throwable {
		waitForElementToClickable(btnSetting);
		btnSetting.click();
	}

	public void clickOnbtnAdminSettings() throws Throwable {
		waitForElementToClickable(btnAdminSettings);
		btnAdminSettings.click();
	}
	public void entertxtPLCTeamName(String PLCTeamName) throws Throwable {
		waitForElementToClickable(txtPLCTeamName);
		txtPLCTeamName.click();
		txtPLCTeamName.clear();
		txtPLCTeamName.sendKeys(PLCTeamName);
		txtPLCTeamName.sendKeys(Keys.TAB);
	}
	
	public void selectTeam(String strContentArea) {
		waitForElementToAppear(liContentArea);	
		TestUtil.selectItemByVisibleText(liContentArea, strContentArea);
	}
	
	public void clickandSelectContentArea(String strContentArea) throws Throwable {
		waitForElementToAppear(liContentArea);	
		liContentArea.click();
		smallwaitDriver();
		String xPathforTeam = "//li[normalize-space()='" + strContentArea + "']";
		try {
			
			pbDriver.findElement(By.xpath(xPathforTeam)).click();
		}catch(NoSuchElementException e) {
			System.out.println(e.getLocalizedMessage());	
		}
	}
	
	public void clickOnbtnradioSETDAYS(String strradioSETDAYS) throws Throwable {
		waitForElementToClickable(btnradioSETDAYS);
		btnradioSETDAYS.click();
	}
	
	public void clickOnbtnAlarmclock() throws Throwable {
		waitForElementToClickable(btnAlarmclock);
		btnAlarmclock.click();
	}

	public void entertxtTEAMNORMS(String TEAMNORMS) throws Throwable {
		waitForElementToClickable(txtTEAMNORMS);
		txtTEAMNORMS.sendKeys(TEAMNORMS);
		txtTEAMNORMS.sendKeys(Keys.TAB);
	}
	
	public void clickOnbtnNEXT() throws Throwable {
		//waitForElementToClickable(btnNEXT);
		btnNEXT.click();
		waitForPageLoaded();
	}
	// PLC Team Set Up
	
		public void createPLCTeamSetUp(String pPLCTeamName,String pradioSETDAYS,String pstrContentArea, String pTEAMNORMS,String pstrStartMeetingTime, String pstrEndMeetingTime ) throws Throwable {
			waitDriver();
			
			entertxtPLCTeamName(pPLCTeamName);
			smallwaitDriver();
			clickandSelectContentArea(pstrContentArea);
			smallwaitDriver();
			clickOnbtnradioSETDAYS(pradioSETDAYS);
			smallwaitDriver();
			
			entertxtStartMeetingTime(pstrStartMeetingTime);
			entertxtEndMeetingTime(pstrEndMeetingTime);
			entertxtTEAMNORMS(pTEAMNORMS);
			
			clickOnbtnNEXT();
		}
		
		// PLC Team Set Up
		
			public void createMultiplePLCTeamSetUp(String pPLCTeamName,String pstrContentArea, String pTEAMNORMS,String pstrStartMeetingTime, String pstrEndMeetingTime ) throws Throwable {
				waitDriver();
				
				entertxtPLCTeamName(pPLCTeamName);
				smallwaitDriver();
				clickandSelectContentArea(pstrContentArea);
				smallwaitDriver();
				//clickOnbtnradioSETDAYS(pradioSETDAYS);
				smallwaitDriver();
				
				entertxtStartMeetingTime(pstrStartMeetingTime);
				entertxtEndMeetingTime(pstrEndMeetingTime);
				entertxtTEAMNORMS(pTEAMNORMS);
				
				clickOnbtnNEXT();
			}
		/*
		public void createPLCTeamSetUpWithOutValues() throws Throwable {
			waitDriver();			
			clickOnbtnNEXT();
		}
		*/
		//23-06-2021
		public void entertxtStartMeetingTime(String strStartMeetingTime) throws Throwable {
			waitForElementToClickable(txtStartMeetingTime);
			txtStartMeetingTime.sendKeys(strStartMeetingTime);
			txtStartMeetingTime.sendKeys(Keys.TAB);
		}
		
		public void entertxtEndMeetingTime(String strEndMeetingTime) throws Throwable {
			waitForElementToClickable(txtEndMeetingTime);
			txtEndMeetingTime.sendKeys(strEndMeetingTime);
			txtEndMeetingTime.sendKeys(Keys.TAB);
		}
		
		//24/06/2021
		////span[normalize-space()='Sohail'] Select membeers
		
		public void selectMember(String strMemebers) throws Throwable {
			
			smallwaitDriver();
			String[] array = strMemebers.split(",");
		      for(String value:array) {
		         System.out.print(value + " ");
		         
		         String xPathforMember = "//span[normalize-space()='" + value + "']";
					try {
						
						pbDriver.findElement(By.xpath(xPathforMember)).click();
					}catch(NoSuchElementException e) {
						System.out.println(e.getLocalizedMessage());	
					}
					
		      }			
			
		}
		
		//multiple days
		
public void selectDays(String strDays) throws Throwable {
			
			smallwaitDriver();
			String[] array = strDays.split(",");
		      for(String value:array) {
		         System.out.print(value + " ");
		         
		         String xPathforDays = "//p[normalize-space()='" + value + "']";
					try {
						
						pbDriver.findElement(By.xpath(xPathforDays)).click();
					}catch(NoSuchElementException e) {
						System.out.println(e.getLocalizedMessage());	
					}
					
		      }			
			
		}
		
		 //28/06/2021
		//button[@aria-label='move selected right']
	    @FindBy(xpath ="//button[@aria-label='move selected right']")
	  	private WebElement btnMoveSelectedRight;  
	    
	    @FindBy(xpath ="//button[@aria-label='move all right']")
		private WebElement btnMoveAllSelectedRight;
	    
	    @FindBy(xpath ="//button[@aria-label='move selected left']")
		private WebElement btnMoveSelectedLeft;
	    
	    @FindBy(xpath ="//button[@aria-label='move all left']")
		private WebElement btnMoveAllSelectedLeft;
	    
	    @FindBy(xpath ="//body/div[@id='root']/div/div/main/div/div/div/div/div/div/div/div/div/div/div/button[2]")
		private WebElement btn2ndNext;
	    
	    @FindBy(xpath ="//div[@id='root']//div//div//main//div//div//div//div//div//div//div//div//div//div//div//div//div[@role='button']")
		private WebElement liSelectRole;
	    
	    @FindBy(xpath ="//body/div[@id='root']/div/div/main/div/div/div/div/div/div/div/div/div/div/div/button[2]")
		private WebElement btnFinish;
	    
	    @FindBy(xpath ="//div[@role='alert']")
		private WebElement msgAlert;
	    
	    
	    @FindBy(xpath ="//div[@role='alert']")
		private WebElement lblModalMessages;
	    
	    public void clickOnbtnMoveSelectedRight() throws Throwable {
			waitForElementToClickable(btnMoveSelectedRight);
			btnMoveSelectedRight.click();
		}
	    
	    public void clickOnbtnMoveAllSelectedRight() throws Throwable {
			waitForElementToClickable(btnMoveAllSelectedRight);
			btnMoveAllSelectedRight.click();
		}
	    
	    public void clickOnbtnMoveSelectedLeft() throws Throwable {
			waitForElementToClickable(btnMoveSelectedLeft);
			btnMoveSelectedLeft.click();
		}
	    
	    public void clickOnbtnMoveAllSelectedLeft() throws Throwable {
			waitForElementToClickable(btnMoveAllSelectedLeft);
			btnMoveAllSelectedLeft.click();
		}
	    
	    public void clickOnbtnbtn2ndNext() throws Throwable {
			waitForElementToClickable(btn2ndNext);
			btn2ndNext.click();
			waitForPageLoaded();
		}
	    
	    public void clickandSelectRole(String strRole) throws Throwable {
	    	
	    	smallwaitDriver();
			String[] array = strRole.split(",");
			int counter = 1;
		      for(String value:array) {
		         System.out.print(value + " ");
		         
		         String liSelectRoleXpath = "//div/div/div/div/div/div[" + counter + "]/div[2]/div[1]/div[1]/div[1]";
		         try {
						
						pbDriver.findElement(By.xpath(liSelectRoleXpath)).click();
						counter = counter + 1 ;
					}catch(NoSuchElementException e) {
						System.out.println(e.getLocalizedMessage());	
					}
		         
		         smallwaitDriver();
					String xPathforTeam = "//li[normalize-space()='" + value + "']";
					try {
						
						pbDriver.findElement(By.xpath(xPathforTeam)).click();
					}catch(NoSuchElementException e) {
						System.out.println(e.getLocalizedMessage());	
					}
		         
		      }
			
			
		}
	    
	    
	    public void clickOnbtnbtnFinish() throws Throwable {
			waitForElementToClickable(btnFinish);
			btnFinish.click();
		}
	    
	    public String getMessage() {
	    	waitForElementToAppear(msgAlert);
	    	return msgAlert.getText();
	    }
		

	  //Team
	  				@FindBy(xpath ="//p[normalize-space()='Please Enter Team Name.']")
	  			    private WebElement lblTeamNameMandatory;
	  				
	  				@FindBy(xpath ="//p[normalize-space()='Please Select Content Area.']")
	  			    private WebElement lblContentMandatory;
	  				
	  				
	  				
	  				@FindBy(xpath ="//p[contains(text(),'Please Select Days.')]")
	  			    private WebElement lblDaysMandatory;
	  				
	  				@FindBy(xpath ="//p[contains(text(),'Please Select Start Time.')]")
	  			    private WebElement lblStartTimeMandatory;
	  				
	  				@FindBy(xpath ="//p[contains(text(),'Please Select End Time.')]")
	  			    private WebElement lblEndTimeMandatory;
	  				
	  				public String getTextforTeamNameField() {
	  					waitForElementToAppear(lblTeamNameMandatory);
	  					return lblTeamNameMandatory.getText();
	  				}
	  				
	  				public String getTextforContentField() {
	  					waitForElementToAppear(lblContentMandatory);
	  					return lblContentMandatory.getText();
	  				}
	  				
	  				
	  				
	  				public String getTextforDaysField() {
	  					waitForElementToAppear(lblDaysMandatory);
	  					return lblDaysMandatory.getText();
	  				}
	  				
	  				public String getTextforStartTimeField() {
	  					waitForElementToAppear(lblStartTimeMandatory);
	  					return lblStartTimeMandatory.getText();
	  				}
	  				public String getTextforEndTimeField() {
	  					waitForElementToAppear(lblEndTimeMandatory);
	  					return lblEndTimeMandatory.getText();
	  				}
	  		public void createPLCTeamSetUpWithOutValues() throws Throwable {
	  			waitDriver();			
	  			clickOnbtnNEXT();
	  		}



			public String getTeamCreatedMessages() {
				waitForElementToAppear(lblModalMessages);
				return lblModalMessages.getText();
			}

			 //Add Roles
			 @FindBy(xpath ="//p[normalize-space()='Add New Role']")
			    private WebElement btnAddNewRole;
			 
			 public void clickOnbtnAddNewRole() {
					waitForElementToAppear(btnAddNewRole);
					btnAddNewRole.click();
				}
			 @FindBy(xpath ="//input[@placeholder='Set Role Name']")
				private WebElement txtRoleTitle;
			 
			 public void entertxtRoleTitle(String RoleTitle) throws Throwable {
					waitForElementToClickable(txtRoleTitle);
					txtRoleTitle.click();
					txtRoleTitle.clear();
					txtRoleTitle.sendKeys(RoleTitle);
					txtRoleTitle.sendKeys(Keys.TAB);
				} 
			 
			 @FindBy(xpath ="//span[normalize-space()='Create']")
			    private WebElement btnCreate;
			 
			 public void clickOnbtnCreate() {
					waitForElementToAppear(btnCreate);
					btnCreate.click();
}
}