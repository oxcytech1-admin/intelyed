package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import base.PageBase;
import utils.TestUtil;

public class PLCminutesPage extends PageBase {
	
	
	@FindBy(xpath ="//div[@id='demo-simple-select-outlined']")
    private WebElement liSelectTeams;
	
	@FindBy(xpath ="//div[@role='tabpanel']//div//p//div//div//div//button[@type='button']")
    private WebElement btnNewMinutes;
	
	@FindBy(xpath ="//span[@title='My Teams']//*[local-name()='svg']")
    private WebElement lnkMyTeams;
	
	@FindBy(xpath ="//body//div[@id='root']//div//div//div//div//div//div//ul//div//div[3]")
    private WebElement lnkPLCminutes;
	
	@FindBy(xpath ="//body/div[@role='presentation']/div/div/div[2]/div[1]/button[1]/p[1]")
    private WebElement btnAddEditMembers;
	
	@FindBy(xpath ="//p[normalize-space()='Add Topic']")
    private WebElement btnAddTopic;
	
	@FindBy(xpath ="//div[8]//div[1]//button[1]//p[1]")
    private WebElement btnAddTask;
	
	@FindBy(xpath ="//p[normalize-space()='Add Achievements']")
    private WebElement btnAddAchievements;
	
	@FindBy(xpath ="//button[@type='button']//span//p")
    private WebElement btnSaveChanges;
	
	@FindBy(xpath ="//body/div[@role='presentation']/div/div/div/div/div//*[local-name()='svg']")
    private WebElement btnBackArrow;
	
	@FindBy(xpath ="//input[@id='[object Object]']")
    private WebElement txtNotes;
	
	@FindBy(xpath ="//textarea[@id='[object Object]']")
    private WebElement txtTask;
	
	@FindBy(xpath ="//textarea[@id='0']")
    private WebElement txtAchievements;
	
	@FindBy(xpath ="//div[9]//div[1]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement selectDueDate;
	
	@FindBy(xpath ="//div[@role='presentation']//div//div//div//div//div//div//button[@type='button']//p")
    private WebElement btnViewOwner;
	
	@FindBy(xpath ="//p[normalize-space()='Save for Later']")
    private WebElement btnSaveForLater;
	
	@FindBy(xpath ="//button[@type='button']//span//p")
    private WebElement btnSave;
	
	@FindBy(xpath ="//button[@type='button']//span//p")
    private WebElement btnCancel;
	

	@FindBy(xpath ="//div[@role='alert']")
	private WebElement msgUpdationAlert;
	
	public  PLCminutesPage(WebDriver driver) {
		setWebDriver(driver);
	}
	
	public void clickOnbtnNewMinutes() throws Throwable {
		waitDriver();
		waitForElementToAppear(btnNewMinutes);
		btnNewMinutes.click();
		
	}
	
	public void clickOnlnklnkMyTeams() {
		waitForElementToAppear(lnkMyTeams);
		lnkMyTeams.click();
	}
	
	public PLCminutesPage clickOnlnkPLCminutes() {
		waitForElementToAppear(lnkPLCminutes);
		lnkPLCminutes.click();
		return new PLCminutesPage(pbDriver);
		
	}
	
	public void clickOnbtnAddEditMembers() {
		btnAddEditMembers.click();
	}
	
	public void clickOnbtnAddTopic() {
		btnAddTopic.click();
	}
	
	public void clickOnbtnAddTask() {
		btnAddTask.click();
	}
	
	public void clickOnbtnAddAchievements() {
		btnAddAchievements.click();
	}
	
	public void clickOnbtnSaveChanges() {
		btnSaveChanges.click();
	}
	
	public void clickOnbtnBackArrow() {
		btnBackArrow.click();
	}
	
	public void enterNotes(String strNotes) {
		waitForElementToAppear(txtNotes);
		txtNotes.clear();
		txtNotes.sendKeys(strNotes);
	}
	
	public void enterAchievements(String strAchievements) {
		waitForElementToAppear(txtAchievements);
		txtAchievements.clear();
		txtAchievements.sendKeys(strAchievements);
	}
	
	public void enterTask(String strTask) {
		waitForElementToAppear(txtTask);
		txtTask.clear();
		txtTask.sendKeys(strTask);
	}
	
	public void clickOnbtnViewOwner() {
		waitForElementToAppear(btnViewOwner);
		btnViewOwner.click();
	}
	//select due date
	
	
	
	
	public void clickOnbtnSaveForLater() {
		btnSaveForLater.click();
	}
	
	public void clickOnbtnSave() {
		btnSave.click();
		waitForPageLoaded();
	}
	
	public void clickOnbtnCancel() {
		btnCancel.click();
	}
	
	//30/06/2021
	@FindBy(xpath ="//div[1]/div[1]/div[2]/button[2]")
    private WebElement btnRemoveMember;
	
	@FindBy(xpath ="//div[1]/div[3]/div[2]/div[1]/div[1]/div[2]/button[1]")
    private WebElement btnMarkPresent;
	
	@FindBy(xpath ="//div[@role='none presentation']//button[2]")
    private WebElement btnYesPresent;
	
	@FindBy(xpath ="//div[normalize-space()='Select Role']")
    private WebElement liSelectRole;
	
	
	@FindBy(xpath ="//div[2]/div[1]/div[1]/fieldset[1]/label[1]")
    private WebElement radioSelectOwner;
	
	@FindBy(xpath ="//span[contains(text(),'Save')]")
    private WebElement btnSaveSelectOwner;
	
	public void clickbtnYesForPresent() {
		waitForElementToAppear(btnYesPresent);
		btnYesPresent.click();
	}
	
	
	public void clickOnbtnMarkPresentforOneMember() {
		List<WebElement> records = pbDriver.findElements(By.xpath("//body/div[@role='presentation']/div/div/div/div[2]/div"));
		if(records.size() > 1) {
			waitForElementToAppear(btnRemoveMember);
			btnRemoveMember.click();
		}
		btnMarkPresent.click();
		clickbtnYesForPresent();
	}
	
	public void clickOnradioSelectOwner() {
		waitForElementToAppear(radioSelectOwner);
		radioSelectOwner.click();
	}
	
	public void clickbtnSaveSelectOwner() {
		waitForElementToAppear(btnSaveSelectOwner);
		btnSaveSelectOwner.click();
	}
	
	 public void clickandSelectRole(String strRole) throws Throwable {
			waitForElementToAppear(liSelectRole);	
			liSelectRole.click();
			smallwaitDriver();
			String xPathforTeam = "//li[normalize-space()='" + strRole + "']";
			try {
				
				pbDriver.findElement(By.xpath(xPathforTeam)).click();
			}catch(NoSuchElementException e) {
				System.out.println(e.getLocalizedMessage());	
			}
		}
	 
	 public void clickandSelectTeam(String strTeam) throws Throwable {
			waitForElementToAppear(liSelectTeams);	
			liSelectTeams.click();
			smallwaitDriver();
			String xPathforTeam = "//li[normalize-space()='" + strTeam + "']";
			try {
				WebElement webElementTeam = pbDriver.findElement(By.xpath(xPathforTeam));
				jsExecutorscrollIntoView(webElementTeam);
				pbDriver.findElement(By.xpath(xPathforTeam)).click();
				waitForPageLoaded();
				waitDriver();
			}catch(NoSuchElementException e) {
				System.out.println(e.getLocalizedMessage());	
			}
		}
	
	
	/*public void removeAllFromList() {
		List<WebElement> records = pbDriver.findElements(By.xpath("//body/div[@role='presentation']/div/div/div/div[2]/div"));
		
		for(int i = 1; i < records.size(); i++) {
			try {
				String xPathForRemove = pbDriver.findElement(By.xpath(xPathForYear));
				pbDriver.findElement(By.xpath(xPathForYear)).click();
			}catch(NoSuchElementException e) {
				System.out.println(e.getLocalizedMessage());	
			}
		}
		smallwaitDriver();
		
	}
*/	
	// PLC Minutes
		
			public void createPLCMinutes(String pstrTeam,String pstrRole,String pstrNotes, String pstrTask, String strAchievements ) throws Throwable {
				waitDriver();
				
				clickandSelectTeam(pstrTeam);
				smallwaitDriver();
				clickOnbtnNewMinutes();
				smallwaitDriver();
				clickOnbtnAddEditMembers();
				smallwaitDriver();
				clickOnbtnMarkPresentforOneMember();
				clickandSelectRole(pstrRole);
				clickOnbtnBackArrow();
				enterNotes(pstrNotes);
				clickOnbtnAddTask();
				enterTask(pstrTask);
				clickOnbtnViewOwner();
				clickOnradioSelectOwner();
				clickbtnSaveSelectOwner();
				clickOnbtnAddAchievements();
				enterAchievements(strAchievements);
				clickOnbtnSave();
			}

			 public String getUpdationAlert() {
			    	waitForElementToAppear(msgUpdationAlert);
			    	return msgUpdationAlert.getText();
			    }
			 
			 
				
}
