package intelyEDtestcases;


import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;
import base.TestBase;
import pages.AchievementPage;
import pages.DashboardPage;
import pages.HomePage;
import pages.IntelyChargePage;
import pages.LoginPage;
import utils.ExcelDataDrivenTest;


public class TC010_Create_New_Achievement_WithoutValues extends TestBase {

	@Test
	public void TC010_Create_New_Achievement() throws Throwable {
		
		ArrayList<String> TestcaseName;
		ExcelDataDrivenTest d = new ExcelDataDrivenTest();
		TestcaseName =d.getData("TC010_Create_New_Achievement_WithoutValues");
        LoginPage pglogin = new LoginPage(driver);
		
		//Step1 Login to the website
        HomePage pgHomePage = pglogin.intelyEDLogin(TestcaseName.get(1), TestcaseName.get(2));
		try {
			Assert.assertNotNull(pgHomePage);
			reporting("Login Validation", "User should able to login", TestcaseName.get(1) + " ", "Pass");
			
		}catch(Throwable e) {
			reporting("Login Validation", "User should log in", "Login Failed with user" + TestcaseName.get(1), "Fail");
			throw new AssertionError("Login Validation failure error message", e);
		}
		
		//Step 2 Click on Achievement link
		pgHomePage.clickOnlnkMySchool();
		AchievementPage pgAchievementPage = pgHomePage.clickOnlnkAchievement();
		try {
			
			//Step 3 Create Achievement
			pgAchievementPage.addAchievementWithoutInputValues();
			reporting("Create Achivement", "User should not able to create achivement", "User not able to create achivement ", "Pass");
		}catch(Throwable e) {
			reporting("Create Achivement", "User should not able to create achivement", "User created achivement without team, descripttion and Title ", "Fail");
			throw new AssertionError("Create Achivement Validation failure error message", e);
		}
		
		//Asserting Team 
		
		try {
			Assert.assertEquals(pgAchievementPage.getTextforTeamField(), "Please Select Team");
			//Assert.assertNotEquals(pglogin.getConfirmationMessage(), "Username/Password is invalid");
			reporting("Team Field Validation", "User should not able to see the * sign if no value entered ", "Actul Value " + pgAchievementPage.getTextforTeamField(), "Pass");
			
		}catch(Throwable e) {
			reporting("Team Field Validation", "User should not able to see the * sign if no value entered ", "Actul Value " + pgAchievementPage.getTextforTeamField(), "Fail");
			throw new AssertionError("Date Validation failure error message", e);
		}
		
		//Asserting Title
		
		try {
			Assert.assertEquals(pgAchievementPage.getTextforTitleField(), "Please Enter Title");
			//Assert.assertNotEquals(pglogin.getConfirmationMessage(), "Username/Password is invalid");
			reporting("Title Field Validation", "Expected Value * ", "Actul Value " + pgAchievementPage.getTextforTitleField(), "Pass");
			
		}catch(Throwable e) {
			reporting("Title Field Validation", "Expected Value * ", "Actul Value " + pgAchievementPage.getTextforTitleField(), "Fail");
			throw new AssertionError("Date Validation failure error message", e);
		}
		
		//Asserting Description
		
		try {
			Assert.assertEquals(pgAchievementPage.getTextforDescField(), "Please Enter Description");
			//Assert.assertNotEquals(pglogin.getConfirmationMessage(), "Username/Password is invalid");
			reporting("Description Field Validation", "Expected Value * ", "Actul Value " + pgAchievementPage.getTextforDescField(), "Pass");
			
		}catch(Throwable e) {
			reporting("Description Field Validation", "Expected Value * ", "Actul Value " + pgAchievementPage.getTextforDescField(), "Fail");
			throw new AssertionError("Date Validation failure error message", e);
		}
		
		
	
		
		pgAchievementPage.clickOnbtnCancel();
		Thread.sleep(5000);

				

	}	
		
}
//