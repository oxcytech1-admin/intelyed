package testcases;


import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;
import base.TestBase;
import pages.AchievementPage;
import pages.DashboardPage;
import pages.HomePage;
import pages.IntelyChargePage;
import pages.LoginPage;
import pages.PLCTeamSetupPage;
import utils.ExcelDataDrivenTest;


public class TC011_PLC_Record_Achivements extends TestBase {

	@Test
	public void TC011_PLC_Teams() throws Throwable {
		
		ArrayList<String> TestcaseName;
		ExcelDataDrivenTest d = new ExcelDataDrivenTest();
		TestcaseName =d.getData("TC011_PLC_Record_Achivements");
        LoginPage pglogin = new LoginPage(driver);
        PLCTeamSetupPage pgPLCTeamSetupPage;
		//Step1 Login to the website
        HomePage pgHomePage = pglogin.intelyEDLogin(TestcaseName.get(1), TestcaseName.get(2));
		try {
			Assert.assertNotNull(pgHomePage);
			reporting("Login Validation", "User should able to login", TestcaseName.get(1) + " ", "Pass");
			
		}catch(Throwable e) {
			reporting("Login Validation", "User should log in", "Login Failed with user" + TestcaseName.get(1), "Fail");
			throw new AssertionError("Login Validation failure error message", e);
		}
		//Step 2    Click on settings
		try {
		PLCTeamSetupPage pgPLCTeamSetupPage1 =  pgHomePage.clickOnbtnSettings();
		pgHomePage.clickOnbtnSettings();
		reporting("Click on Setting Validation", "User should able to click on New Minutes", "User clicked on New Minutest button ", "Pass");
		}catch(Throwable e) {
			reporting("Click on Setting Validation", "User should not be able to click on New Minutes", "User not able to click on New Minutest button " + e.getLocalizedMessage(), "Fail");
			throw new AssertionError("New Click Minutes Validation error message", e);
		}
		
		//Step 3 "Content Area from the dropbox."
		try {
			
		pgPLCTeamSetupPage.clickandSelectContentArea("Maths");
		reporting("Validate Content Area from the dropbox", "User should able to Validate Content Area from the dropbox", "User able to select Content Area from the dropbox", "Pass");
		}catch(Throwable e) {
			reporting("Validate Content Area from the dropbox", "User should not be able to Validate Content Area from the dropbox", "User not able to select Content Area from the dropbox" + e.getLocalizedMessage(), "Fail");
			throw new AssertionError("New Click Minutes Validation error message", e);
		}
		//Step 4	"Checked the SET DAYS.	// Click on any of the radio buttons."
		try {
		pgPLCTeamSetupPage.clickOnbtnradioSETDAYS();
		reporting("Validate Checked the SET DAYS", "User should able to Validate Checked the SET DAYS", "User able to Checked the SET DAYS", "Pass");
		}catch(Throwable e) {
			reporting("Validate Checked the SET DAYS", "User should not be able to Checked the SET DAYS", "User not able to Checked the SET DAYS" + e.getLocalizedMessage(), "Fail");
			throw new AssertionError("New Click Minutes Validation error message", e);
		}
		//Step 5	"Click on the Meeting Start Time "
		try {
		pgPLCTeamSetupPage.entertxtStartMeetingTime("17:52");
		//Step 6	"Click on the Meeting End 	Time"
		pgPLCTeamSetupPage.entertxtEndMeetingTime("18:52");
		reporting("Validate Meeting time", "User should able to enter Meeting time", "User able to enter Meeting time", "Pass");
		}catch(Throwable e) {
			reporting("Validate Meeting time", "User should not be able to enter Meeting time", "User not able to enter Meeting time" + e.getLocalizedMessage(), "Fail");
			throw new AssertionError("enter Meeting time Validation error message", e);
		}
		//Step 7	Enter the TEAM NORMS fix error
		try {
		pgPLCTeamSetupPage.entertxtTEAMNORMS("Team norms");
		//Step 8	"Click on NEXT button.		"	//Next Page	
		pgPLCTeamSetupPage.clickOnbtnNEXT();
		reporting("Validate TEAM NORMS and Clicked on NEXT", "User should able to Validate TEAM NORMS and Clicked on NEXT", "User able to Validate TEAM NORMS and Clicked on NEXT", "Pass");
		}catch(Throwable e) {
			reporting("Validate TEAM NORMS and Clicked on NEXT", "User should not be able to Validate TEAM NORMS and Clicked on NEXT", "User not able to Validate TEAM NORMS and Clicked on NEXT" + e.getLocalizedMessage(), "Fail");
			throw new AssertionError("TEAM NORMS and Clicked on NEXT Validation error message", e);
		}
		//Step 9	Search Members
		//Step 10	"ALL MEMBERS		Select the members from checklist"
		//Step 11	Click on the Arrow keys
		//Step 12	"Click on NEXT button.		"
		//Step 13	"Update/change the 		PLC TEAM ROLES from the 		dropdown."
		//Step 14	"Click on Finish button.		"

		
		Thread.sleep(5000);

				
		}
	}	
		

